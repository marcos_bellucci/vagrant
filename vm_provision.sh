#!/bin/bash

echo ' ***** installing essentials ***** '
bash /vagrant/.vm_provision_scripts/essentials.sh

echo ' ***** installing nginx      ***** '
bash /vagrant/.vm_provision_scripts/nginx.sh

echo ' ***** installing nodejs     ***** '
bash /vagrant/.vm_provision_scripts/nodejs.sh

echo ' ***** installing rbenv      ***** '
bash /vagrant/.vm_provision_scripts/rbenv.sh

echo ' ***** installing postgres   ***** '
bash /vagrant/.vm_provision_scripts/postgres.sh
