#!/bin/bash

sudo -s

curl http://nginx.org/keys/nginx_signing.key > nginx_signing.key
apt-key add nginx_signing.key

a=`sudo grep nginx /etc/apt/sources.list`
if [ ${#a} -gt 1 ]; then # avoid duplicated entry
  codename=`awk -F '=' '/CODENAME/ {print $2}' /etc/lsb-release`
  echo "deb http://nginx.org/packages/ubuntu/ $codename nginx"     >> /etc/apt/sources.list
  echo "deb-src http://nginx.org/packages/ubuntu/ $codename nginx" >> /etc/apt/sources.list 
fi

apt-get update
apt-get -y install nginx

#check if installed correctly
if which nginx > /dev/null; then
  echo 'Nginx correctly installed'
else
  echo 'Nginx failed to install'
fi
