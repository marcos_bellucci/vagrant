#!/bin/bash

curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs

#check if installed correctly
if which nodejs > /dev/null; then
  echo 'Nodejs correctly installed'
else
  echo 'Nodejs failed to install'
fi

