#!/bin/bash

apt-get purge -y ruby

su vagrant << 'EOF'
  cd ~
  rm -rf .rbenv

  #rbenv: manage multiple ruby versions
  git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
  echo 'eval "$(rbenv init -)"' >> ~/.bashrc
  export PATH="$HOME/.rbenv/bin:$PATH"
  eval "$(rbenv init -)"

  #ruby-build
  git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
  echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
  export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"

  #check if installed correctly
  if which ruby-build > /dev/null; then
    echo 'ruby-build installed'
  else
    echo 'ruby-build failed to install'
  fi

  #rehash: runs automatically rbenv rehash every time a gem is installed
  git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash

  rbenv install 2.2.2
  rbenv global 2.2.2

  #check if installed correctly
  if which ruby > /dev/null; then
    echo 'Ruby correctly installed'
    ruby -v
  else
    echo 'Ruby failed to install'
  fi

  #Not install gem documentation
  echo "gem: --no-ri --no-rdoc" > ~/.gemrc
  gem install bundler
EOF
