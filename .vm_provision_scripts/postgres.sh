#!/bin/bash

sudo apt-get install -y postgresql postgresql-contrib

#check if installed correctly
if which psql > /dev/null; then
  echo 'Postgres correctly installed'
else
  echo 'Postgres failed to install'
fi

#gem install pg -- --with-pg-config=/usr/bin/pg_config
#export PGPASSWORD='pgpassword'
#psql -h 'server name' -U 'user name' -d 'base name' \
#       -c 'command' 

#sudo -u postgres psql
#create user rails_db with password 'password';
#alter role rails_db superuser createrole createdb replication;
#create database rails_project_production owner rails_db;
