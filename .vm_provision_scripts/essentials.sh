#!/bin/bash

#Avoid dpkg-preconfigure error
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales

sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get install -y build-essential zlib1g-dev git-core sqlite3 libsqlite3-dev autoconf bison libssl-dev libyaml-dev libreadline6-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev


