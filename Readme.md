# Vagrant for rails

This Vagrant configuration comes with some common configuration for your rails environment.
  - Ubuntu 12.4 Trusty
  - Rbenv which installs Ruby 2.2.2
  - Nginx as a web server
  - Postgres as database
  - Nodejs as javascript interpreter
  - Some essentials which contains Git and some needed dependencies

# Installation

  First you would need to get vagrant installed.
  ```bash
  $ cd vagrant 
  $ vagrant up
  ```

# Why using a vagrant environment
It is basically an environment where you can break everything.
Are you nervous because you don't know what is going to happen when installing a new service on the production server ?
Posibly you cant't try in your staging server since users are working on it.
Just try out in your own server.

# What is next ?
  Once you have your vagrant running you may want to deploy to it.
    That requires some extra configuration. For that i recommend [TalkingQuickly capistrano template](https://github.com/TalkingQuickly/capistrano-3-rails-template). Which faces the responsability of configure servicies to work toghether when you make a deploy.
      
